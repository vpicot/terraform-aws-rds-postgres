locals {
  zone_lower        = lower(var.security)
  environment_lower = lower(var.environment)
  company_lower     = lower(var.company)
  service_id_lower  = lower(var.service_id)
  freetext_lower    = var.pitr_restore_time != null || var.use_latest_restorable_time != null ? "${lower(var.freetext)}-restored" : lower(var.freetext)

  identifier              = "${local.company_lower}-${local.region_map_naming[var.availability_zone]}-${local.az_map_naming[var.availability_zone]}-${local.zone_lower}-${local.environment_lower}-${local.service_id_lower}-${local.freetext_lower}"
  restore_from_identifier = var.identifier_db_restore != null ? var.identifier_db_restore : "${local.company_lower}-${local.region_map_naming[var.availability_zone]}-${local.az_map_naming[var.availability_zone]}-${local.zone_lower}-${local.environment_lower}-${local.service_id_lower}-${lower(var.freetext)}"
  restore_data = {
    source_db_instance_identifier = var.identifier_db_restore != null ? var.identifier_db_restore : "${local.company_lower}-${local.region_map_naming[var.availability_zone]}-${local.az_map_naming[var.availability_zone]}-${local.zone_lower}-${local.environment_lower}-${local.service_id_lower}-${lower(var.freetext)}"
    pitr_restore_time             = var.pitr_restore_time
    pitr_restore_to_latest        = var.use_latest_restorable_time
  }

  region_map_naming = {
    eu-west-1a = "eu1"
    eu-west-1b = "eu1"
    eu-west-1c = "eu1"
    eu-west-3a = "eu2"
    eu-west-3b = "eu2"
    eu-west-3c = "eu2"
  }
  az_map_naming = {
    eu-west-1a = "aza"
    eu-west-1b = "azb"
    eu-west-1c = "azc"
    eu-west-3a = "aza"
    eu-west-3b = "azb"
    eu-west-3c = "azc"
  }
  required_tags = {
    Security    = upper(var.security)
    Environment = upper(var.environment)
    Company     = upper(var.company)
    ServiceID   = upper(var.service_id)
    Name        = lower(local.identifier)
  }
}
