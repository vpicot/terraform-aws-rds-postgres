locals {
  db_instance_address              = concat(aws_db_instance.postgresql_instance.*.address, [""])[0]
  db_instance_arn                  = concat(aws_db_instance.postgresql_instance.*.arn, [""])[0]
  db_instance_availability_zone    = concat(aws_db_instance.postgresql_instance.*.availability_zone, [""])[0]
  db_instance_endpoint             = concat(aws_db_instance.postgresql_instance.*.endpoint, [""])[0]
  db_instance_hosted_zone_id       = concat(aws_db_instance.postgresql_instance.*.hosted_zone_id, [""])[0]
  db_instance_id                   = concat(aws_db_instance.postgresql_instance.*.id, [""])[0]
  db_instance_resource_id          = concat(aws_db_instance.postgresql_instance.*.resource_id, [""])[0]
  db_instance_status               = concat(aws_db_instance.postgresql_instance.*.status, [""])[0]
  db_instance_name                 = concat(aws_db_instance.postgresql_instance.*.name, [""])[0]
  db_instance_username             = concat(aws_db_instance.postgresql_instance.*.username, [""])[0]
  db_instance_port                 = concat(aws_db_instance.postgresql_instance.*.port, [""])[0]
  db_instance_ca_cert_identifier   = concat(aws_db_instance.postgresql_instance.*.ca_cert_identifier, [""])[0]
  db_instance_domain               = concat(aws_db_instance.postgresql_instance.*.domain, [""])[0]
  db_instance_domain_iam_role_name = concat(aws_db_instance.postgresql_instance.*.domain_iam_role_name, [""])[0]
  db_identifier                    = concat(aws_db_instance.postgresql_instance.*.identifier, [""])[0]

}

output "db_identifier" {
  description = "The identifier of the RDS instance"
  value       = local.db_identifier
}
output "db_instance_address" {
  description = "The address of the RDS instance"
  value       = local.db_instance_address
}

output "db_instance_arn" {
  description = "The ARN of the RDS instance"
  value       = local.db_instance_arn
}

output "db_instance_availability_zone" {
  description = "The availability zone of the RDS instance"
  value       = local.db_instance_availability_zone
}

output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = local.db_instance_endpoint
}

output "db_instance_hosted_zone_id" {
  description = "The canonical hosted zone ID of the DB instance (to be used in a Route 53 Alias record)"
  value       = local.db_instance_hosted_zone_id
}

output "db_instance_id" {
  description = "The RDS instance ID"
  value       = local.db_instance_id
}

output "db_instance_resource_id" {
  description = "The RDS Resource ID of the DB instance"
  value       = local.db_instance_resource_id
}

output "db_instance_status" {
  description = "The RDS instance status"
  value       = local.db_instance_status
}

output "db_instance_name" {
  description = "The database name"
  value       = local.db_instance_name
  sensitive   = true
}

output "db_instance_username" {
  description = "The master username for the database"
  value       = local.db_instance_username
  sensitive   = true
}

output "db_instance_port" {
  description = "The database port"
  value       = local.db_instance_port
  sensitive   = true
}

output "db_instance_ca_cert_identifier" {
  description = "Specifies the identifier of the CA certificate for the DB instance"
  value       = local.db_instance_ca_cert_identifier
}

output "db_instance_domain" {
  description = "The ID of the Directory Service Active Directory domain the instance is joined to"
  value       = local.db_instance_domain
}

output "db_instance_domain_iam_role_name" {
  description = "The name of the IAM role to be used when making API calls to the Directory Service. "
  value       = local.db_instance_domain_iam_role_name
}
