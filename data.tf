data "aws_availability_zone" "az_id" {
  name = var.availability_zone
}

data "aws_region" "current" {}

// Retrieve a VPC dynamically
data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Security"
    values = [upper(var.security)]
  }

  filter {
    name   = "tag:Company"
    values = [upper(var.company)]
  }
}

// Retrieve subnets within a VPC dynamically
data "aws_subnets" "subnets_private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Security"
    values = [upper(var.security)]
  }

  filter {
    name   = "tag:Environment"
    values = [upper(var.environment)]
  }
}

data "aws_subnet" "subnet_private" {
  for_each = toset(data.aws_subnets.subnets_private.ids)
  id       = each.value
}

# data "vault_generic_secret" "vault" {
#   path       = var.vault_path
#   depends_on = [vault_generic_secret.secret]
# }
