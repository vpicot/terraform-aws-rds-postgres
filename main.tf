

resource "aws_security_group" "allow_tcp" {
  count       = var.db_instance_security_group_ids == null ? 1 : 0
  name        = "sgp-${local.identifier}"
  description = "Allow TCP inbound traffic"
  vpc_id      = data.aws_vpc.vpc.id

  ingress {
    description     = "SqlServer access"
    from_port       = var.port
    to_port         = var.port
    protocol        = "tcp"
    cidr_blocks     = var.application_servers_security_group_ids != null ? null : [for s in data.aws_subnet.subnet_private : s.cidr_block]
    security_groups = var.application_servers_security_group_ids != null ? var.application_servers_security_group_ids : null
  }

  tags = merge(
    {
      Name = "sgp-${local.identifier}"
    },
  local.required_tags)
}

resource "aws_db_subnet_group" "subnet_group" {
  name        = "subnet-group-dbiaas-${local.identifier}"
  description = "Database subnet group for dbiaas-${local.identifier}"
  subnet_ids  = data.aws_subnets.subnets_private.ids

  tags = merge(
    {
      "Name" = "subnet-group-dbiaas-${local.identifier}"
    },
  local.required_tags)
}

resource "aws_db_parameter_group" "enable_query_logging" {
  name   = "parameter-group-dbiaas-${local.identifier}"
  family = "postgres14"

  parameter {
    name  = "log_statement"
    value = "ddl"
  }

  parameter {
    name  = "log_min_duration_statement"
    value = "1"
  }
}

resource "aws_db_instance" "postgresql_instance" {
  #checkov:skip=CKV_AWS_118:Ensure that enhanced monitoring is enabled for Amazon RDS instance (Defined in variables, default is none, but should be set for prod)
  #checkov:skip=CKV_AWS_157:Ensure that RDS instances have Multi-AZ enabled (Defined in variables, default is false, but should be defined to true for prod)
  #checkov:skip=CKV_AWS_149:Ensure that Secrets Manager secret is encrypted using KMS CMK (should be defined)
  #checkov:skip=CKV_AWS_161:Ensure RDS database has IAM authentication enabled (optional)

  identifier = "dbiaas-${local.identifier}"

  engine            = "postgres"
  engine_version    = var.replicate_source_db == null ? var.engine_version : null
  instance_class    = var.instance_class
  allocated_storage = var.allocated_storage
  storage_type      = var.storage_type
  storage_encrypted = true
  kms_key_id        = var.kms_key_arn

  db_name  = var.pitr_restore_time != null || var.use_latest_restorable_time != null ? null : replace("dbiaas-${local.identifier}", "-", "")
  username = var.replicate_source_db == null ? var.username : null
  # password = data.vault_generic_secret.vault.data["password"]
  password = "foobarbaz"
  port     = var.port

  replicate_source_db = var.replicate_source_db

  snapshot_identifier = var.snapshot_identifier

  vpc_security_group_ids = var.db_instance_security_group_ids == null ? [aws_security_group.allow_tcp[0].id] : var.db_instance_security_group_ids
  db_subnet_group_name   = aws_db_subnet_group.subnet_group.name
  parameter_group_name   = aws_db_parameter_group.enable_query_logging.name
  option_group_name      = var.option_group_name

  availability_zone   = var.availability_zone
  multi_az            = var.multi_az
  iops                = var.iops
  publicly_accessible = var.publicly_accessible
  monitoring_interval = var.monitoring_interval
  monitoring_role_arn = var.monitoring_interval > 0 ? var.monitoring_role_arn : null

  allow_major_version_upgrade = var.allow_major_version_upgrade
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  apply_immediately           = var.apply_immediately
  maintenance_window          = var.maintenance_window
  skip_final_snapshot         = var.skip_final_snapshot
  copy_tags_to_snapshot       = var.copy_tags_to_snapshot
  final_snapshot_identifier   = "dbiaas-${local.identifier}-final-snapshot-${replace(timestamp(), ":", "-")}"
  max_allocated_storage       = var.max_allocated_storage

  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_retention_period = var.performance_insights_enabled == true ? var.performance_insights_retention_period : null

  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window

  character_set_name = var.character_set_name

  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports

  deletion_protection = var.deletion_protection

  iam_database_authentication_enabled = var.iam_database_authentication_enabled

  dynamic "restore_to_point_in_time" {
    for_each = var.pitr_restore_time != null || var.use_latest_restorable_time != null ? ["restoration"] : []
    content {
      source_db_instance_identifier = local.restore_from_identifier
      restore_time                  = var.pitr_restore_time
      use_latest_restorable_time    = var.use_latest_restorable_time
    }

  }

  tags = merge(
    {
      "Name" = "dbiaas-${local.identifier}"
    },
  local.required_tags)
  timeouts {
    create = lookup(var.timeouts, "create", null)
    delete = lookup(var.timeouts, "delete", null)
    update = lookup(var.timeouts, "update", null)
  }

  lifecycle {
    ignore_changes = [
      final_snapshot_identifier,
      backup_window,
      backup_retention_period
    ]
  }
  # lifecycle {
  #   ignore_changes = [
  #     backup_window,
  #     backup_retention_period
  #   ]
  #   precondition {
  #     condition     = lower(var.zone) == "z1" ? lower(var.environment) == "dev" : true
  #     error_message = "If the subnet zone is z1, the env should be dev."
  #   }

  #   precondition {
  #     condition     = lower(var.zone) == "z15" ? lower(var.environment) == "npd" : true
  #     error_message = "If the subnet zone is z15, the env should be npd."
  #   }

  #   precondition {
  #     condition     = lower(var.zone) == "z2" || lower(var.zone) == "z3" ? lower(var.environment) == "prd" : true
  #     error_message = "If the subnet zone is z2 or z3, the env should be prd."
  #   }

  #   precondition {
  #     condition     = !(var.pitr_restore_time != null && var.use_latest_restorable_time != null)
  #     error_message = "pitr_restore_time and use_latest_restorable_time cannot be both set."
  #   }
  # }
}


