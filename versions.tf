terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.20.1"
    }
    # vault = {
    #   source  = "hashicorp/vault"
    #   version = "3.8.1"
    # }
  }
}
