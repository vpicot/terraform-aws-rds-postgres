variable "security" {
  type        = string
  description = "The desired zone to provision the instance (zone urbasec)"
  # validation {
  #   condition     = contains(["z1", "z2", "z3", "z15"], lower(var.security))
  #   error_message = "Must be one of these: z1, z2, z3, z15."
  # }
}

variable "environment" {
  type        = string
  description = "DEV/NPD/PRD"
  # validation {
  #   condition     = contains(["DEV", "NPD", "PRD"], var.environment)
  #   error_message = "Must be one of these: DEV, NPD, PRD."
  # }
}

variable "company" {
  type        = string
  description = "The trigram of the company"
  # validation {
  #   condition     = length(var.company) == 3
  #   error_message = "Must be 3 characters long."
  # }
}

variable "service_id" {
  type        = string
  description = "The service portfolio"
}

variable "freetext" {
  type        = string
  description = "Freetext to add to the db instance identfier"
}

variable "identifier_db_restore" {
  type        = string
  description = "The identifier of a db you want to restore"
  default     = null
}


variable "pitr_restore_time" {
  description = "The date and time to restore from. Value must be a time in Universal Coordinated Time (UTC) format and must be before the latest restorable time for the DB instance. Can't be set if pitr_restore_to_latest is set"
  type        = string
  default     = null
}

variable "use_latest_restorable_time" {
  description = "Restore to latest backup. Can't be set if pitr_restore_time is set"
  type        = string
  default     = null
}

variable "allocated_storage" {
  description = "The allocated storage in gigabytes"
  type        = string
  default     = "20"
}

variable "storage_type" {
  description = "One of 'standard' (magnetic), 'gp2' (general purpose SSD), or 'io1' (provisioned IOPS SSD). The default is 'io1' if iops is specified, 'standard' if not. Note that this behaviour is different from the AWS web console, where the default is 'gp2'."
  type        = string
  default     = "gp2"
}

variable "kms_key_arn" {
  description = "The ARN for the KMS encryption key. If creating an encrypted replica, set this to the destination KMS ARN. If storage_encrypted is set to true and kms_key_arn is not specified the default KMS key created in your account will be used"
  type        = string
  default     = null
}

variable "replicate_source_db" {
  description = "Specifies that this resource is a Replicate database, and to use this value as the source database. This correlates to the identifier of another Amazon RDS Database to replicate."
  type        = string
  default     = null
}

variable "snapshot_identifier" {
  description = "Specifies whether or not to create this database from a snapshot. This correlates to the snapshot ID you'd find in the RDS console, e.g: rds:production-2015-06-26-06-05."
  type        = string
  default     = null
}

variable "engine_version" {
  description = "The engine version to use"
  type        = string
  default     = "14.2"
}

variable "instance_class" {
  description = "The instance type of the RDS instance"
  type        = string
  default     = ""
}

variable "name" {
  description = "The DB name to create. If omitted, no database is created initially"
  type        = string
  default     = ""
}

variable "create_db_name_parameter" {
  description = "Whether to create or not the DB Name Paramater in SSM (for eg to POSTGRESQL, the DB Name should be null)."
  type        = bool
  default     = true
}
variable "db_name_parameter_description" {
  description = "The Description of the Parameter holding DB name to create."
  type        = string
  default     = ""
}
variable "username" {
  description = "Username for the master DB user"
  type        = string
  default     = "admin_postgre"
}
variable "secret_name_prefix" {
  description = "Creates a unique name beginning with the specified prefix. Conflicts with secret_name"
  type        = string
  default     = ""
}
variable "secret_description" {
  description = "A description of the secret"
  type        = string
  default     = "RDS DB Password"
}
variable "secret_policy" {
  description = "A valid JSON document representing a resource policy."
  type        = string
  default     = null
}
variable "secret_recovery_window_in_days" {
  description = "Specifies the number of days that AWS Secrets Manager waits before it can delete the secret. This value can be 0 to force deletion without recovery or range from 7 to 30 days. The default value is 30"
  type        = number
  default     = 30
}
variable "enable_secret_rotation" {
  description = "Whether to enable Secret Rotation or not"
  type        = bool
  default     = false
}
variable "secret_rotation_lambda_arn" {
  description = "Specifies the ARN of the Lambda function that can rotate the secret."
  type        = string
  default     = ""
}
variable "secret_rotation_frequency" {
  description = "Specifies the number of days between automatic scheduled rotations of the secret."
  type        = number
  default     = 90
}
variable "secret_string" {
  description = "Specifies text data that you want to encrypt and store in this version of the secret. This is required if secret_binary is not set."
  type        = string
  default     = ""
}
variable "generate_secret" {
  description = "Whether to generate the RDS DB Password as a secret or not."
  type        = bool
  default     = true
}
variable "secret_length" {
  description = "Secret Length. Defaults to 32."
  type        = number
  default     = 32
}
variable "secret_min_lower_chars" {
  description = "Minimum number of lowercase alphabet characters in the result. Defaults to 4"
  type        = number
  default     = 4
}
variable "enable_upper_chars" {
  description = "Whether to enable of not upper characters in the generated secret."
  type        = bool
  default     = true
}
variable "secret_min_upper_chars" {
  description = "Minimum number of uppercase alphabet characters in the result. Defaults to 4"
  type        = number
  default     = 4
}
variable "enable_numeric_chars" {
  description = "Whether to enable or not numeric characters in the generated secret."
  type        = bool
  default     = true
}
variable "secret_min_numeric_chars" {
  description = "Minimum number of Numeric Characters in the generated password. Defaults to 4"
  type        = number
  default     = 4
}
variable "enable_special_chars" {
  description = "Whether to enable or not special characters in the password."
  type        = bool
  default     = true
}
variable "min_special_chars" {
  description = "Minimum number of Special Characters in the generated password. Defaults to 4"
  type        = number
  default     = 4
}
variable "override_special_chars" {
  description = "Supply your own list of special characters to use for string generation. This overrides the default character list in the special argument. The special argument must still be set to true for any overwritten characters to be used in generation."
  type        = string
  default     = "#$%^*()-=_+[]{};<>?,."
}
variable "db_username_parameter_description" {
  description = "The Description of the Parameter holding DB Username to create."
  type        = string
  default     = "Username for DB Connection"
}
variable "port" {
  description = "The port on which the DB accepts connections"
  type        = string
  default     = "5432"
}
variable "db_port_parameter_description" {
  description = "The Description of the Parameter holding DB Port to create."
  type        = string
  default     = "Port for DB connection"
}
variable "final_snapshot_identifier" {
  description = "The name of your final DB snapshot when this DB instance is deleted."
  type        = string
  default     = null
}

variable "application_servers_security_group_ids" {
  description = "List of VPC security groups, where applications servers are to allow access to the db. If set a security group allowing this security group to access the db will be created, optional, you  can't set it if db_instance_security_group_ids is set"
  type        = list(string)
  default     = null
}
variable "db_instance_security_group_ids" {
  description = "List of VPC security groups to associate to db instance, optional, you  can't set it if application_servers_security_group_ids is set"
  type        = list(string)
  default     = null
}

variable "parameter_group_name" {
  description = "Name of the DB parameter group to associate"
  type        = string
  default     = ""
}

variable "availability_zone" {
  description = "The Availability Zone of the RDS instance"
  type        = string
  default     = "eu-west-3a"
}

variable "multi_az" {
  description = "Specifies if the RDS instance is multi-AZ"
  type        = bool
  default     = false
}

variable "iops" {
  description = "The amount of provisioned IOPS. Setting this implies a storage_type of 'io1'"
  type        = number
  default     = 0
}

variable "publicly_accessible" {
  description = "Bool to control if instance is publicly accessible"
  type        = bool
  default     = false
}

variable "monitoring_interval" {
  description = "The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB instance. To disable collecting Enhanced Monitoring metrics, specify 0. The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60."
  type        = number
  default     = 0
  validation {
    condition     = contains([0, 1, 5, 10, 15, 30, 60], var.monitoring_interval)
    error_message = "Must be one of these: 0, 1, 5, 10, 15, 30, 60."
  }
}

variable "monitoring_role_arn" {
  description = "The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs. Must be specified if monitoring_interval is non-zero."
  type        = string
  default     = ""
}

variable "monitoring_role_name" {
  description = "Name of the IAM role which will be created when create_monitoring_role is enabled."
  type        = string
  default     = "rds-monitoring-role"
}

variable "create_monitoring_role" {
  description = "Create IAM role with a defined name that permits RDS to send enhanced monitoring metrics to CloudWatch Logs."
  type        = bool
  default     = false
}

variable "allow_major_version_upgrade" {
  description = "Indicates that major version upgrades are allowed. Changing this parameter does not result in an outage and the change is asynchronously applied as soon as possible"
  type        = bool
  default     = false
}

variable "auto_minor_version_upgrade" {
  description = "Indicates that minor engine upgrades will be applied automatically to the DB instance during the maintenance window"
  type        = bool
  default     = true
}

variable "apply_immediately" {
  description = "Specifies whether any database modifications are applied immediately, or during the next maintenance window"
  type        = bool
  default     = false
}

variable "maintenance_window" {
  description = "The window to perform maintenance in. Syntax: 'ddd:hh24:mi-ddd:hh24:mi'. Eg: 'Mon:00:00-Mon:03:00'"
  type        = string
  default     = "Mon:00:00-Mon:03:00"
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted. If true is specified, no DBSnapshot is created. If false is specified, a DB snapshot is created before the DB instance is deleted, using the value from final_snapshot_identifier"
  type        = bool
  default     = false
}

variable "copy_tags_to_snapshot" {
  description = "On delete, copy all Instance tags to the final snapshot (if final_snapshot_identifier is specified)"
  type        = bool
  default     = false
}

variable "backup_retention_period" {
  description = "The days to retain backups for"
  type        = number
  default     = 31
}

variable "backup_window" {
  description = "The daily time range (in UTC) during which automated backups are created if they are enabled. Example: '09:46-10:16'. Must not overlap with maintenance_window"
  type        = string
  default     = "03:00-06:00"
}

variable "additional_tags" {
  description = "Additional optional tags to tag the resources accordingly."
  type        = map(string)
  default     = {}
}

variable "option_group_name" {
  description = "Name of the DB option group to associate."
  type        = string
  default     = ""
}

variable "character_set_name" {
  description = "(Optional) The character set name to use for DB encoding in Oracle instances. This can't be changed. See Oracle Character Sets Supported in Amazon RDS for more information"
  type        = string
  default     = ""
}

variable "enabled_cloudwatch_logs_exports" {
  description = "List of log types to enable for exporting to CloudWatch logs. If omitted, no logs will be exported. Valid values (depending on engine): alert, audit, error, general, listener, slowquery, trace, postgresql (PostgreSQL), upgrade (PostgreSQL)."
  type        = list(string)
  default     = ["postgresql"]
}

variable "timeouts" {
  description = "(Optional) Updated Terraform resource management timeouts. Applies to `aws_db_instance` in particular to permit resource management times"
  type        = map(string)
  default = {
    create = "40m"
    update = "80m"
    delete = "40m"
  }
}

variable "deletion_protection" {
  description = "The database can't be deleted when this value is set to true."
  type        = bool
  default     = false
}

variable "performance_insights_enabled" {
  description = "Specifies whether Performance Insights are enabled"
  type        = bool
  default     = false
}

variable "performance_insights_retention_period" {
  description = "The amount of time in days to retain Performance Insights data. Either 7 (7 days) or 731 (2 years)."
  type        = number
  default     = 7
}

variable "max_allocated_storage" {
  description = "Specifies the value for Storage Autoscaling"
  type        = number
  default     = 0
}

variable "ca_cert_identifier" {
  description = "Specifies the identifier of the CA certificate for the DB instance"
  type        = string
  default     = "rds-ca-2019"
}

variable "delete_automated_backups" {
  description = "Specifies whether to remove automated backups immediately after the DB instance is deleted"
  type        = bool
  default     = false
}

variable "iam_database_authentication_enabled" {
  description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled"
  type        = bool
  default     = false
}

# variable "vault_path" {
#   description = "vault path"
#   type        = string
# }